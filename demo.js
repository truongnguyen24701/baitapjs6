var numberArr = [];
function themso() {
    var numberValue = document.getElementById("txt-number").value;
    numberArr.push(Number(numberValue));

    document.getElementById("txt-number").value = " "; // lúc user nhập xong tự trả về ô dữ liệu trống
    document.getElementById("ket-qua").innerHTML = numberArr;
}

//bài1
function tongsoduong() {
    var sum = 0;
    for (var i = 0; i < numberArr.length; i++) {
        sum += numberArr[i];
    }
    console.log("Tổng số dương: ", sum);
    document.getElementById("ket-qua-so-duong").innerHTML = `Tổng số dương: ${sum}`;
}

//bài 2
function demso() {
    var soduong = 0;
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] >= 1) {
            soduong += 1;
        }
    }
    console.log("Đếm số dương:", soduong);
    document.getElementById("ket-qua-dem-so").innerHTML = `Đếm số dương: ${soduong}`;
}


//bài3
function timsonhonhat() {
    var min = numberArr[0];
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] < min) {
            min = numberArr[index]
        }
    }
    console.log("Số nhỏ nhât:", min);
    document.getElementById("ket-qua-so-nho-nhat").innerHTML = `<div> Số nhỏ nhất: ${min} </div>`;
}


//bài 4
function timsoduongnhonhat() {
    if (numberArr.length == 0) {
        console.log("Không có số dương nào trong mảng");
        document.getElementById("ket-qua-so-duong-nho-nhat").innerHTML = `Không có số dương nào trong mảng`
    } else {
        var minDuong = numberArr[0];
        for (var index = 0; index < numberArr.length; index++) {
            if (numberArr[index] >= 1) {
                minDuong = numberArr[index]
            }
        }
        console.log("Số dương nhỏ nhất", minDuong);
        document.getElementById("ket-qua-so-duong-nho-nhat").innerHTML = `Số dương nhỏ nhất: ${minDuong}`
    }
}


//bài 5
function timsochan() {
    var soChan = 0;
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] % 2 == 0) {
            soChan = numberArr[index]
        }
    }
    console.log("Số chẵn cuối cùng", soChan);
    document.getElementById("ket-qua-so-chan").innerHTML = `Số chẵn cuối cùng: ${soChan}`
}


//bài 6 
function doicho() {
    var index1 = Number(document.getElementById("txt-1").value);
    var index2 = Number(document.getElementById("txt-2").value);

    var so1 = numberArr[index1];
    var so2 = numberArr[index2];

    numberArr[index1] = so2;
    numberArr[index2] = so1;

    console.log(numberArr);
    document.getElementById("ket-qua-doi-cho").innerHTML = numberArr;
}

//bài 7
function sapxep() {
    numberArr.sort(function (a, b) {
        return a - b;
    });
    console.log(numberArr);
    document.getElementById("ket-qua-sap-xep").innerHTML = numberArr;
}

//bài 8
function timsonguyento() {
    if (numberArr.length == 0) {
        console.log("-1");
        document.getElementById("ket-qua-so-nguyen-to").innerHTML = `-1`;
    } else {
        var soNguyenTo = 0;
        for (var index = 0; index < numberArr.length; index++) {
            if (numberArr[index] >= 2) {
                soNguyenTo = numberArr[index];
                break;
            }
        }
    }
    console.log(soNguyenTo);
    document.getElementById("ket-qua-so-nguyen-to").innerHTML = soNguyenTo;
}

//bài 9
var soNguyen = [];
function themnumber() {
    var soNguyenValue = document.getElementById("txt-so-nguyen").value;
    soNguyen.push(Number(soNguyenValue));

    document.getElementById("txt-so-nguyen").value = " "; // lúc user nhập xong tự trả về ô dữ liệu trống
    document.getElementById("ket-qua-them-so").innerHTML = soNguyen;
}

function demsonguyen() {
    var tongSonguyen = 0;
    for (var index = 0; index < soNguyen.length; index++) {
        if (Number.isInteger(soNguyen[index])) {
            tongSonguyen++
        }
    }
    console.log("Đếm số nguyên:", (tongSonguyen));
    document.getElementById("ket-qua-dem-so-nguyen").innerHTML = `Đếm số nguyên: ${tongSonguyen}`;
}

//bài 10
function sosanh() {
    var soDuong = 0;
    var soAm = 0;
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] < 0) {
            soAm++
        } else {
            soDuong++
        }
    }
    if (soDuong < soAm) {
        console.log("Số Dương < Số Âm");
        document.getElementById("ket-qua-so-sanh").innerHTML = `Số Dương < Số Âm`
    } else if (soDuong > soAm) {
        console.log("Số Dương > Số Âm");
        document.getElementById("ket-qua-so-sanh").innerHTML = `Số Dương > Số Âm`

    } else {
        console.log("Số Dương = Số Âm");
        document.getElementById("ket-qua-so-sanh").innerHTML = `Số Dương = Số Âm`
    }
}